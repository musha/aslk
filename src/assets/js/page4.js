// console.log('page4 page');

export default {
    data() {
        return {
            //
        };
    },
    mounted() {
        var context = this;
        //
        var tl = new TimelineMax();
        tl.from($('.page-page4'), 0.4, { autoAlpha: 0, onComplete: function() {
            btn($('.btn-back'));
            btn($('.btn-next'));
        } }, 0.1);

        $('#mv-page4').on('play', function() {
            musicOff();
        });
        $('#mv-page4').on('pause', function() {
            musicOn();
        });
        $('#mv-page4').on('error ended', function() {
            musicOn();
        });

        // btn
        function btn(el) {
            var tl = new TimelineMax({ repeat: -1, repeatDelay: 0, yoyo:true });
            tl.to(el, 0.6, { scale: 1.1, ease: Power0.easeNone });
        };
    },
    methods: {
        // back
        back() {
            this.$router.replace('/page3');
        },
        // next
        next() {
            this.$router.replace('/page5');
        }
    }
};
