// console.log('result page');

export default {
	data() {
		return {
			//
		};
	},
	mounted() {
		var context = this;
		//
		var tl = new TimelineMax();
		tl.from($('.page-result'), 0.4, { autoAlpha: 0, onComplete: function() {
            btn($('.btn-back'));
            btn($('.btn-home'));
        } }, 0.1);

		// btn
        function btn(el) {
            var tl = new TimelineMax({ repeat: -1, repeatDelay: 0, yoyo:true });
            tl.to(el, 0.6, { scale: 1.1, ease: Power0.easeNone });
        };
	},
	methods: {
		// back
        back() {
            this.$router.replace('/page5');
        },
        // home
        home() {
            this.$router.replace('/home');
        }
	}
};
