// console.log('home page');

export default {
	data() {
		return {
			//
		};
	},
	mounted() {
		var context = this;
		//
		var tl = new TimelineMax();
		tl.from($('.page-home'), 0.2, { autoAlpha: 0, onComplete: function() {
            start();
        } }, 0.1);

		// btn start
        function start() {
            var tl = new TimelineMax({ repeat: -1, repeatDelay: 0, yoyo:true });
            tl.to($('.btn-start'), 0.6, { scale: 1.1, ease: Power0.easeNone });
        };
	},
	methods: {
		//
        start() {
            this.$router.replace('/prize');
        }
	}
};
