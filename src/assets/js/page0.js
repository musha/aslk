// console.log('page0 page');

export default {
	data() {
		return {
			//
		};
	},
	mounted() {
		var context = this;
		//
		var tl = new TimelineMax();
		tl.from($('.page-page0'), 0.4, { autoAlpha: 0, onComplete: function() {
            next();
            textAn();
        } }, 0.1);

        // video
        // var video = new tvp.VideoInfo(); // 定义视频对象
        // video.setTitle('title'); // 设置视频标题
        // video.setVid('r3015ynw4qo'); // 向视频对象传入视频vid
        // var player = new tvp.Player(); // 定义播放器对象
        // player.create({
        //     width: '100%', // 参数可接受视频高宽数值
        //     height: '100%',
        //     video: video,
        //     modId: 'mv-page0',  // 视频输出的ID 
        //     pic: '', // 定义视频初始页面显示的图片
        //     // playType: '', // 播放器类别
        //     // type: '', // 播放器类别
        //     isHtml5UseAirPlay: true, // Html5 播放器是否使用 Airplay 功能，强烈建议开启
        //     isContinuePlay: false, // 是否续播，默认 true
        //     onwrite: function() {
        //         // console.log('onwrite');
        //     },
        //     oninited: function() {
        //         // console.log('oninited');
        //         $('video').attr('x5-video-player-type', 'h5-page');
        //         $('video').attr('x5-video-player-fullscreen', 'false');
        //     },
        //     onchange: function() {
        //         // console.log('change');
        //     },
        //     onplay: function() {
        //         // console.log('onplay');
        //     },
        //     onplaying: function() {
        //         // console.log('onplaying');
        //         musicOff();
        //     },
        //     onallended: function() {
        //         // console.log('onallended');
        //         musicOn();
        //     },
        //     onpause: function() {
        //         // console.log('onpause');
        //         musicOn();
        //     },
        //     onresume: function() {
        //         // console.log('onresume');
        //         musicOff();
        //     },
        //     onfullscreen: function() {
        //         // console.log('onfullscreen');
        //         // musicOff();
        //     }
        // });
        $('#mv-page0').on('play', function() {
            musicOff();
        });
        $('#mv-page0').on('pause', function() {
            musicOn();
        });
        $('#mv-page0').on('error ended', function() {
            musicOn();
        });

		// btn next
        function next() {
            var tl = new TimelineMax({ repeat: -1, repeatDelay: 0, yoyo:true });
            tl.to($('.btn-rush'), 0.6, { scale: 1.1, ease: Power0.easeNone });
        };

        // text an
        function textAn() {
    		var tl = new TimelineMax({});
            tl.to($('.text1'), 0.3, { autoAlpha: 1 });
            tl.to($('.text2'), 0.3, { autoAlpha: 1 }, '+=0.4');
            tl.to($('.text3'), 0.3, { autoAlpha: 1 }, '+=0.4');
            tl.to($('.text4'), 0.3, { autoAlpha: 1 }, '+=0.4');
            tl.to($('.text5'), 0.3, { autoAlpha: 1 }, '+=0.4');
        }
	},
	methods: {
		//
        rush() {
            this.$router.replace('/page1');
        }
	}
};
