// console.log('prize page');

export default {
	data() {
		return {
			//
		};
	},
	mounted() {
		var context = this;
		//
		var tl = new TimelineMax();
		tl.from($('.page-prize'), 0.4, { autoAlpha: 0, onComplete: function() {
            next();
            textAn();
        } }, 0.1);

		// btn next
        function next() {
            var tl = new TimelineMax({ repeat: -1, repeatDelay: 0, yoyo:true });
            tl.to($('.btn-next'), 0.4, { scale: 1.1, ease: Power0.easeNone });
        };

        // text an
        function textAn() {
            var tl = new TimelineMax({ });
            tl.to($('.btn1'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 });
            tl.to($('.btn2'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
            tl.to($('.btn3'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
            tl.to($('.btn4'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
            tl.to($('.btn5'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
            tl.to($('.btn6'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
            tl.to($('.btn7'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
            tl.to($('.btn8'), 0.2, { autoAlpha: 0, yoyo: true, repeat: 1 }, '+=0.2');
        }
	},
	methods: {
        next() {
            this.$router.replace('/page0');
        },
        modal(e) {
			$('.modal' + e.srcElement.dataset.index).removeClass('lock');
			$('.modal').fadeIn('fast');
        },
        close(e) {
        	$('.modal').fadeOut('fast', function() {
        		$(e.srcElement).addClass('lock');
        	});
        }
	}
};
