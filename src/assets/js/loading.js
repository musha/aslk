// console.log('loading page');
import html5Loader from '../../../static/js/loader/jquery.html5Loader.min.js' // html5loader

export default {
	data() {
		return {
            currentUrl: window.location.href, // 当前地址
            officialUrl: 'http://astrazeneca2020.gypserver.com', // 正式域名
            openid: '',
			source: [
                // {src: require('../img/common/bg.jpg')}
            ]
		};
	},
	mounted() {
		var context = this;

		// 预加载
        var firstLoadFiles = {
            'files': [
                {
                    'type':'VIDEO',
                    'sources': {
                        'h264': {
                            'source': require('../media/page1.mp4'),
                            'size': 2831155.2
                        },
                    }
                },
                {
                    'type':'VIDEO',
                    'sources': {
                        'h264': {
                            'source': require('../media/page2.mp4'),
                            'size': 1677721.6
                        },
                    }
                },
                {
                    'type':'VIDEO',
                    'sources': {
                        'h264': {
                            'source': require('../media/page3.mp4'),
                            'size': 766976
                        },
                    }
                },
                {
                    'type':'VIDEO',
                    'sources': {
                        'h264': {
                            'source': require('../media/page4.mp4'),
                            'size': 2097152
                        },
                    }
                },
                {
                    'type':'VIDEO',
                    'sources': {
                        'h264': {
                            'source': require('../media/page5.mp4'),
                            'size': 902144
                        },
                    }
                },
                {
                    'type': 'AUDIO',
                    'sources': {
                        'mp3': {
                            'source': require('../audio/op.mp3'),
                            'size': 4928307.2
                        }
                    },
                },
                // common
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/bg.jpg'),
                    'size': 125952
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/logo.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/common/border.png'),
                    'size': 17408
                },
                // loading
                {
                    'type': 'IMAGE',
                    'source': require('../img/loading/banner.png'),
                    'size': 49152
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/loading/num.png'),
                    'size': 3072
                },
                // home
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/logo.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/banner.png'),
                    'size': 53248
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/text.png'),
                    'size': 8192
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/home/btn-start.png'),
                    'size': 3072
                },
                // prize
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/bg.jpg'),
                    'size': 55296
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/text.png'),
                    'size': 21504
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/bg-btns.png'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn1.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn2.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn3.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn4.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn5.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn6.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn7.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn8.png'),
                    'size': 2048
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/btn-next.png'),
                    'size': 3072
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal1.png'),
                    'size': 18432
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal2.png'),
                    'size': 12288
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal3.png'),
                    'size': 16384
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal4.png'),
                    'size': 13312
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal5.png'),
                    'size': 19456
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal6.png'),
                    'size': 14336
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal7.png'),
                    'size': 15360
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/prize/modal8.png'),
                    'size': 29696
                },
                // pages
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/bg-page.jpg'),
                    'size': 39936
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/satellite1.png'),
                    'size': 46080
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/satellite2.png'),
                    'size': 39936
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/satellite3.png'),
                    'size': 44032
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/satellite4.png'),
                    'size': 34816
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/satellite5.png'),
                    'size': 45056
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/text1.png'),
                    'size': 17408
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/text2.png'),
                    'size': 15360
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/text3.png'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/text4.png'),
                    'size': 16384
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/text5.png'),
                    'size': 23552
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/page0.jpg'),
                    'size': 34816
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/page1.jpg'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/page2.jpg'),
                    'size': 12288
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/page3.jpg'),
                    'size': 9216
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/page4.jpg'),
                    'size': 11264
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/page5.jpg'),
                    'size': 15360
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/btn-back.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/btn-next.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/pages/btn-rush.png'),
                    'size': 3072
                },
                // result
                {
                    'type': 'IMAGE',
                    'source': require('../img/result/avatar.png'),
                    'size': 15360
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/result/text.png'),
                    'size': 16384
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/result/btn-back.png'),
                    'size': 4096
                },
                {
                    'type': 'IMAGE',
                    'source': require('../img/result/btn-home.png'),
                    'size': 4096
                }
            ]
        };
        $.html5Loader({
            filesToLoad: firstLoadFiles,
            onBeforeLoad: function() {},
            onElementLoaded: function(obj, elm) {},
            onUpdate: function(percentage) {
                // console.log(percentage);
                $('.bar').css({'width': percentage + '%' });
                $('.box-num').css({'left': (percentage * 0.864) + '%' });
                $('.num').html(percentage);
            },
            onComplete: function() {
                setTimeout(function() {
                    context.$router.replace('/home');
                }, 1000);
            }
        });

		var tl = new TimelineMax();
		tl.from($('.page-loading'), 0.2, { autoAlpha: 0, onComplete: function() {
            //
        } }, 0.1);

	},
	methods: {
		//
	}
};