// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// common plugin
import amfe from '../static/js/amfe-flexible.min.js' // 页面分辨率
import { TweenMax, TimelineMax } from 'gsap/TweenMax' //animate

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  mounted() {
  	//
  }
});
