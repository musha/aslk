import Vue from 'vue'
import Router from 'vue-router'
import loading from '@/components/loading'
import home from '@/components/home'
import prize from '@/components/prize'
import page0 from '@/components/page0'
import page1 from '@/components/page1'
import page2 from '@/components/page2'
import page3 from '@/components/page3'
import page4 from '@/components/page4'
import page5 from '@/components/page5'
import result from '@/components/result'

Vue.use(Router)

export default new Router({
  	routes: [
	  	{
	      path: '/',
	      name: 'loading',
	      component: loading
	    },
	    {
	      path: '/home',
	      name: 'home',
	      component: home
	    },
	    {
	      path: '/prize',
	      name: 'prize',
	      component: prize
	    },
	    {
	      path: '/page0',
	      name: 'page0',
	      component: page0
	    },
	    {
	      path: '/page1',
	      name: 'page1',
	      component: page1
	    },
	    {
	      path: '/page2',
	      name: 'page2',
	      component: page2
	    },
	    {
	      path: '/page3',
	      name: 'page3',
	      component: page3
	    },
	    {
	      path: '/page4',
	      name: 'page4',
	      component: page4
	    },
	    {
	      path: '/page5',
	      name: 'page5',
	      component: page5
	    },
	    {
	      path: '/result',
	      name: 'result',
	      component: result
	    }
  	]
})
